Install
------------

1. mkdir ~/.local/share/hamster-time-tracker/
2. cp report_template.html ~/.local/share/hamster-time-tracker/

Note: for Ubuntu 12.04 copy report_template.html to ~/.local/share/hamster-applet/
